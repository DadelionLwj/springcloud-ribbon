package com.qytest.springcloud.controller;

import com.qytest.springcloud.entities.CommonResult;
import com.qytest.springcloud.entities.Payment;
import com.qytest.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author qy
 * @date 2022年07月8日 15:38
 */
@RestController
@RequestMapping("/payment")
@Slf4j
public class PaymentController {
    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @Resource
    private DiscoveryClient discoveryClient;

    @PostMapping(value = "")
    public CommonResult create(@RequestBody Payment payment) {
        try {
            paymentService.save(payment);
            log.info("插入完成");
            return new CommonResult(200, "插入成功,serverPort:" + serverPort, payment);
        } catch (Exception e) {
            return new CommonResult(500, "插入失败", null);
        }
    }

    @GetMapping(value = "/{id}")
    public CommonResult query(@PathVariable("id") Long id) {
        Payment payment = paymentService.getById(id);
        if (payment != null) {
            return new CommonResult(200, "查询成功！serverPort:" + serverPort, payment);
        }
        return new CommonResult(500, "查询失败", null);
    }
    @GetMapping(value ="/discovery")
    public Object discovery(){

        List<String> list=discoveryClient.getServices();
        for(String str:list){
            log.info("====application==={}",str);
        }
       List<ServiceInstance> lt= discoveryClient.getInstances("cloud-PAYMENT");
       for(ServiceInstance instance:lt){
         //  log.info("====instance====="+instance.getHost()+"\t"+instance.getInstanceId()+"\t"+instance.getServiceId()+"\t"+instance.getPort());
       }
       return this.discoveryClient;
    }

}