package com.qytest.springcloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author qy
 * @date 2022年07月08日 17:07
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.qytest"})
@MapperScan(basePackages = {"com.qytest.**.dao"})
//@EnableEurekaClient
@EnableDiscoveryClient
public class CloudProviderPayment {
    public static void main(String[] args) {
        SpringApplication.run(CloudProviderPayment.class, args);
    }
}