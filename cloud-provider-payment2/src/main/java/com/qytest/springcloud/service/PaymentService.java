package com.qytest.springcloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qytest.springcloud.entities.Payment;

/**
 * @author qy
 * @date 2022年07月8日 17:27
 */
public interface PaymentService extends IService<Payment> {
}