package com.qytest.springcloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qytest.springcloud.dao.PaymentMapper;
import com.qytest.springcloud.entities.Payment;
import com.qytest.springcloud.service.PaymentService;
import org.springframework.stereotype.Service;

/**
 * @author qy
 * @date 2022年07月8日 15:38
 */
@Service
public class PaymentServiceImpl extends ServiceImpl<PaymentMapper, Payment> implements PaymentService {

}